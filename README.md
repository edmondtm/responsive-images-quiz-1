Overview

This is a quiz for Responsive Images in Google Developer training. https://developers.google.com/training/web/

The objective of these quizes is to learn how to optimize images to speed up page load.

On the first part of the project is to learn how to resize image display resolution using css and also automate resize actual image resolution using Grunt plugin grunt-responsive-images. By reducing the actual resolution of images and quality, there is no noticabe quality reduction. But we are able to bring down image download from 3MB to only 1.1MB.

On the second part of the project, to further speed up page load, we use fonts to replace the smiley png, inline svg to replace the flourish.png. Instead of using sprite sheet to upload social media links which slows down pageload in HTTP/2 (works in HTTP1), we will use Zocial Icons to insert the icons.

Requirement
As I am using Express Server, Node will be required. On top of that Grunt and ImageMagick.

To Install Grunt
https://gruntjs.com/getting-started

To Install ImageMagick for Linux
http://www.imagemagick.org/script/index.php

To Install ImageMagick for Mac
http://cactuslab.com/imagemagick/

To Run Project
1. Clone Repo
2. Install All Requirements
3. Install all required libraries by running npm i
4. At root of project cloned, run grunt
5. Start project, npm start
6. Open your browser to localhost:3000

